#include <stdio.h>

// used in Unix; process trace; one process controls another
// Our program is controlling and analyzing another program for gadgets.
#include <sys/ptrace.h>  

// for POSIX threading:
#include <unistd.h>

// used in conjunction with pthreads:
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/user.h>
#include <sys/stat.h>

// file descriptor library
#include <fcntl.h>

// for memory management
#include <sys/mman.h>

#include <string.h>
#include <strings.h>

// for error codes
#include <errno.h>

#include <assert.h>
#include <syscall.h>


#include "pstring.h"
#include "misc.h"

// for hash map
#include <map>

// ordered set
#include <set>

using namespace std;


extern "C" void ropstop_mprotect();
extern "C" void ropstop_mprotect_end();

// Error-checking wrapper for ptrace calls.
//
#define PE(c) {                                                               \
  long rv = c;                                                                \
  if ( rv )                                                                   \
    {                                                                         \
      printf("Ptrace error at line %d: %d %s\n",                              \
             __LINE__, errno, strerror(errno));                               \
      abort_tracing = true;                                                   \
    }                                                                         \
 }

// Information about a Gadget
//
struct Gadget {
  int gclass;                   // Classification of gadget, must be 0-63.
  int size;                     // Number of bytes in gadget.
  int *bytes;                   // The gadget 
  int static_matches;           // Number of static visited pages with gadget.
};


enum Gadget_Byte {
  // Note: Values cannot be in the range 0-255
  GB_Wildcard = 500,
  GB_Range
};

// Gadget statistics 
class Gadget_Stat {
  public:
  	Gadget_Stat() { page_cnt = 0;  unlock_cnt = 0; }
  	u_int64_t vector;              
  	int page_cnt;                 // Number of pages with this vector.
  	int unlock_cnt;               // Number of unlocks to such pages.
};


// 
class Gadget_Class_Info {
	public:
  		Gadget_Class_Info() { static_cnt = 0;  name = NULL; }
  		const char *name;
  		int static_cnt;
  		bool avoid_set;               // If true, class part of set to avoid.
};


// Information on a region of memory managed by rop-stop.
//
struct Region_Info {
  size_t base_addr;  // Start of region, rounded to page boundary.
  int size;          // Multiple of pages.

  // Return end address (smallest address after region).
  size_t end_addr() { return base_addr + size; }

  // Return true if ADDR is within region.
  bool in(size_t addr) { return addr >= base_addr && addr < base_addr + size; }
};


// Information on a page managed by rop-stop.
//
//
class Page_Info {
	public:
  		Page_Info(){ inited = false; }
  		bool inited;
  		size_t base_addr;
  		bool is_locked;               // Page locked by rop-stop.
  		int time_stamp;               // Used to choose a page to lock.
  		int unlock_cnt;               // Also used to choose a page to lock.
  		bool page_scanned;            // Page has been scanned for gadgets.
  		u_int64_t gadget_vector;
  		Gadget_Stat *gadget_stat;     
};


// Creating hash maps of Page_Info and Region_Info objects (using their size as key).
typedef map<size_t,Page_Info> Page_Map;
typedef map<size_t,Region_Info> Region_Map;

// Creating iterators for pages and regions.
typedef set<size_t>::iterator Page_Iter;
typedef Region_Map::iterator Region_Iter;

// Creating hash map of Gadget_Stat objects.
typedef map<u_int64_t,Gadget_Stat> Gadget_Stat_Map;
typedef Gadget_Stat_Map::iterator Gadget_Stat_Iter;

// Maximum size of instruction.  This needs to be corrected.
const uint max_insn_size = 20;

class App {
public:
  App();
  void gadgets_init();
  void gadget_insert
  (int gclass, const char *bytes_text, const char *name = NULL); 
  void gadget_body_insert
  (int gclass, const char *bytes_text, const char *name = NULL);
  int start(int argc, char **argv, char **envp);
  int main_loop();
  bool curr_page_process();
  void handle_mmap(size_t addr_start, size_t size, int perm);
  void handle_mprotect(size_t addr_start, size_t size, int perm);

  void mprotect_child(const void *start, size_t size, int prot);

  size_t page_round(size_t addr) { return addr & ~page_mask; }
  bool page_span(size_t addr)
  { return page_size - ( addr & page_mask ) < max_insn_size; }
  void page_scan(Page_Info *pi);
  void page_mp_lock(Page_Info *pi);
  void page_unlock(Page_Info *pi);

  void get_regs();

  bool debug;
  int debug_msg_cnt;
  bool gtune;                   // Print info for tuning gadgets.
  bool show_regions;            // Print memory region locations.
  int iter_cnt;
  int sig_trap_cnt;

  int unlock_page_limit;
  bool never_lock;  // If true never lock pages. For tuning.

  int page_size;  // Size used by memory management.
  int page_mask;

  pid_t pid;                    // PID of program to protect.
  int fd;                       // File descriptor for executable image.

  bool abort_tracing;
  user_regs_struct curr_regs;   // Un-tampered user program registers.
  bool curr_regs_stale;         // curr_regs out of date.
  bool child_modified;          // If true, code injected into child.
  size_t child_save_text;       // Original value of child overwritten by..

  // Regions of memory that have been locked and for which
  // there are no entries in pages_seen_m.
  Region_Map regions_locked;
  Page_Map pages_seen_m;        // Pages that had been unlocked.
  set<size_t> pages_unlocked;   // Pages currently unlocked.
  int curr_time;                // Timestamp applied to pages.
  size_t inj_addr_safe;         // Place where code can be injected.

  // Approximate number of regions kernel needs to keep track of for child.
  int prot_count;

  PStack<Gadget> gadgets;       // Gadgets to scan for.
  u_int64_t gadget_avoid_mask;  // Mask identifying set of classes to avoid. 

  Gadget_Stat_Map gadget_vec_stat;
  Gadget_Stat_Map gadget_stat;
  Gadget_Class_Info gadget_class_info[64];
  //  int gadget_class_static_cnt[64];
  int min_gadget; // Smallest gadget class number.
  int max_gadget; // Largest gadget class number.

  // Map holding system call names. Used for debugging.
  map<int,const char*> syscall_name;
  void syscall_name_insert(int num, const char* name);

  // Return locked region containing addr, or NULL if addr not in a
  // locked region.
  //
  Region_Info* region_lookup(size_t addr);

  // Create a new locked region.
  //
  Region_Info& region_new(size_t start_addr, size_t end_addr);

  // Remove an existing locked region.
  //
  void region_remove(size_t start_addr, int size);

  // Get or create a page info structure and initialize it.
  //
  Page_Info& page_get(size_t addr)
  {
    Page_Info& pi = pages_seen_m[addr];
    if ( !pi.inited )
      {
        pi.inited = true;
        pi.base_addr = addr;
        pi.page_scanned = false;
        pi.gadget_vector = 0;
        pi.is_locked = false;
        pi.time_stamp = 0;
        pi.unlock_cnt = 0;
      }
    return pi;
  }

  // For performance measurement and tuning.
  //
  double time_wall_start;

  // Added in avg_cnt_samples and avg_count.
  int unlock_count, lock_count, scan_count, avg_cnt_samples, avg_count;
  int mprotect_child_count;
};


App::App()
{
  debug = false;
  debug_msg_cnt = 10;
  gtune = false;
  show_regions = false;
  abort_tracing = false;
  fd = -1;
  scan_count = 0;
  unlock_count = 0;
  lock_count = 0;
  avg_cnt_samples = 0;
  avg_count = 0;
  prot_count = 0;
  sig_trap_cnt = 0;
  mprotect_child_count = 0;
  child_modified = false;
  min_gadget = 64;
  max_gadget = -1;

  // Obtain system call names. (Works on RHEL 6, does not work on F 20.)
#undef _ASM_X86_UNISTD_64_H
#undef __SYSCALL
#define __SYSCALL(a,b) syscall_name_insert(a,#b);
#include <asm/unistd_64.h>
#undef __SYSCALL

}

__attribute__ ((noinline)) void
App::syscall_name_insert(int num, const char* name)
{
  syscall_name[num] = name;
}


// Get child register values.
//
void
App::get_regs()
{
  if ( !curr_regs_stale ) return;
  
  // using wrapper for ptrace calls
  // Get the child registers using the ID of the process being traced, and store to curr_regs.  
  PE( ptrace(PTRACE_GETREGS,pid,NULL,&curr_regs) ) ;
  curr_regs_stale = false;
}



Region_Info*
App::region_lookup(size_t addr)
{
  // Find the address of the upper bound of the locked region. 
  Region_Iter ir = regions_locked.upper_bound(addr);
  if ( ir == regions_locked.begin() ) return NULL;
  
  // Decrement the upper address of locked region.
  ir--;
  
  
  Region_Info* const ri = &ir->second;
  return ri->in(addr) ? ri : NULL;
}



Region_Info&
App::region_new(size_t start_addr, size_t end_addr)
{
  const size_t start_rp = page_round(start_addr);
  const size_t end_rp = page_round(end_addr+page_size-1);

  Region_Iter ir = regions_locked.upper_bound(start_rp);
  Region_Info* const ri_after = ir == regions_locked.end() ? NULL : &ir->second;
  const bool overlap_after = ri_after && ri_after->base_addr < end_rp;
  const bool new_first = ir == regions_locked.begin();
  if ( !new_first ) ir--;
  Region_Info* const ri_before = new_first ? NULL : &ir->second;
  const bool overlap_before = ri_before && ri_before->in(start_rp);

  if ( overlap_before )
    pError_Msg("Internal error: new region overlap before ...\n"
               "  %#zx - %#zx    %#zx - %#zx\n",
               ri_before->base_addr, ri_before->end_addr(),
               start_rp, end_rp);
  if ( overlap_after )
    pError_Msg("Internal error: new region overlap after ...\n"
               "  %#zx - %#zx    %#zx - %#zx\n",
               start_rp, end_rp,
               ri_after->base_addr, ri_after->end_addr());


  Region_Info& ri = regions_locked[start_rp];
  // Get base address of page region.
  ri.base_addr = start_rp;
  
  // Find the number of pages in the region.
  const int num_pages = ( end_rp - start_rp ) / page_size;
  
  // Find and return the size of the region.
  ri.size = num_pages * page_size;
  return ri;
}

// Examine mmap call that child just made, update locked regions if needed.
//
void
App::handle_mmap(size_t addr_start, size_t size, int perms)
{
  if ( addr_start == 0 ) return;
  const size_t addr_end = addr_start + size;

  region_remove(addr_start,size);

  if ( perms & 0x4 )
    {
      Region_Info& ri = region_new(addr_start,addr_end);

      if ( show_regions )
        printf("New Region 0x%zx - 0x%zx (%d pages)\n",
               ri.base_addr, ri.base_addr + ri.size,
               ri.size/page_size);
#if 1
      mprotect_child((void*)ri.base_addr, ri.size, PROT_READ);
#else
      // Code to investigate performance issues and resource
      // exhaustion problems by calling mprotect_child more times
      // than is necessary.
      const int pages = ri.size / page_size;

      for ( int i=0; i<pages; i += 1 )
        {
          const size_t start_addr = ri.base_addr + i * page_size;
          
          mprotect_child((void*)start_addr, page_size, PROT_READ);
        }
      return;
#endif
    }
}


void
App::region_remove(size_t addr_start, int size)
{
  const size_t start_rp = page_round(addr_start);
  const size_t end_rp = page_round(start_rp + size + page_size - 1 );

  while ( true )
    {
      Region_Iter ir = regions_locked.lower_bound(start_rp);
      if ( ir == regions_locked.end() ) break;
      Region_Info ri_cpy = ir->second;
      if ( ri_cpy.base_addr >= end_rp ) break;
      regions_locked.erase(ir);
      const bool whole_region = ri_cpy.end_addr() <= end_rp;
      if ( show_regions && whole_region )
        printf("Region removed:  %#zx - %#zx\n",
               ri_cpy.base_addr, ri_cpy.end_addr() );
      if ( whole_region ) continue;
      if ( show_regions )
        printf("Region shortened %#zx/%#zx - %#zx\n",
               ri_cpy.base_addr, end_rp,
               ri_cpy.end_addr() );

      region_new(end_rp,ri_cpy.end_addr());
      break;
    }

  while ( true )
    {
      Region_Iter ir = regions_locked.upper_bound(start_rp);
      if ( ir == regions_locked.begin() ) break;
      ir--;
      Region_Info& ri = ir->second;
      if ( !ri.in(start_rp) ) break;
      const int new_size = start_rp - ri.base_addr;
      if ( show_regions )
        printf("Region shortened: %#zx - %#zx/%#zx\n",
               ri.base_addr,
               ri.base_addr + ri.size,
               ri.base_addr + new_size);

      ri.size = new_size;
      break;
    }
}

void
App::mprotect_child(const void *addr, size_t size, int prot)
{
  // Make the mprotect system call in the child process.
  //
  // This system call sets the memory permission bits, of interest
  // to us is turning the execute permission on and off.
  //
  // A system call is made in the child process by copying the code
  // for the call into child memory, and then setting the child PC
  // (IP) to point to it.

  prot_count++;
  mprotect_child_count++;

  get_regs();
  user_regs_struct mod_regs = curr_regs;

  u_int64_t* inj_addr  = (u_int64_t*)inj_addr_safe;
  u_int64_t* payload_8 = (u_int64_t*) ropstop_mprotect;

  // Make a backup of what is already there ...
  //
  if ( !child_modified )
    {
      child_modified = true;
      // Remember original code ...
      child_save_text = ptrace(PTRACE_PEEKTEXT,pid,inj_addr,NULL);
      // ... and overwrite it with our code.
      PE( ptrace(PTRACE_POKETEXT,pid,inj_addr,payload_8[0]) );
    }

  // Set PC (IP) and other registers.
  //
  mod_regs.rax = SYS_mprotect;  // System call number.
  mod_regs.rdi = int64_t(addr); // Address of start of region.
  mod_regs.rsi = size;          // Size of region.
  mod_regs.rdx = prot;          // Permissions to set.
  mod_regs.rip = int64_t(inj_addr);  // Address of our injected code.
  PE( ptrace(PTRACE_SETREGS,pid,NULL,&mod_regs) );

  // Continue execution. Execution will stop when it reaches an int3
  // instruction that we injected.
  PE( ptrace(PTRACE_CONT,pid,NULL,NULL) );

  int status;
  waitpid(pid,&status,0);
  const int sig = WSTOPSIG(status);

  // Check for an error executing the system call.
  //
  const bool problem = !WIFSTOPPED(status) || sig != SIGTRAP;
  if ( problem )
    {
      user_regs_struct regs;
      PE( ptrace(PTRACE_GETREGS,pid,NULL,&regs) );

      siginfo_t sig_info;
      PE( ptrace(PTRACE_GETSIGINFO,pid,NULL,&sig_info) );

      printf("MP wait, status %d, sig %d, PC= %#zx (+%zd), "
             "SP= %#zx, RAX= %#zx\n",
             status,sig,
             size_t(regs.rip),
             size_t(regs.rip) - inj_addr_safe,
             size_t(regs.rsp),size_t(regs.rax));
      printf("  Signal addr: %p   Iter Count %d,  Prot Count %d\n",
             sig_info.si_addr, iter_cnt, prot_count);
      pError_Msg("rop-stop: Problem while trying to set mem protection.\n");
    }

  // Check for an error code from the system call itself.
  //
  user_regs_struct regs;
  PE( ptrace(PTRACE_GETREGS,pid,NULL,&regs) );
  const int rv = regs.rax;

  if ( rv != 0 )
    pError_Msg
      ("MP rax %d for prot of %p - %#zx mode %d\n"
       "   %s\n"
       "   Iter Count %d,  Prot Count %d\n",
       int(regs.rax), addr, size_t(addr) + size, prot,
       -rv == ENOMEM ? "Internal kernel structures could not be allocated." :
       strerror(-rv), iter_cnt, prot_count);
}

#include "gadgets.h"


__attribute__ ((noinline)) void
App::gadget_body_insert(int gclass, const char *bytes_text, const char *name)
{
  pString bytes(bytes_text);
  bytes += " c3";
  gadget_insert(gclass, bytes, name);
}

int
str_to_hex(const char *bp)
{
  // At this point we expect to find two consecutive hex digits.
  // Find out how many consecutive hex digits are actually here.
  const int len = strspn(bp,"0123456789abcdefABCDEF");
  if ( len != 2 ) return -1;

  // Convert ASCII to a number.
  return strtol(bp,NULL,16);
}

__attribute__ ((noinline)) void
App::gadget_insert(int gclass, const char *bytes_text, const char *name)
{
  // Add a gadget to the list.

  if ( gclass < 0 || gclass > 63 )
    {
      fprintf(stderr,"Gadget class %d out of range, should be 0-63.\n",gclass);
      return;
    }

  Gadget_Class_Info* const ci = &gadget_class_info[gclass];
  if ( name && ci->name && !strcmp(name,ci->name) )
    pError_Msg("Gadget class name changed from %s to %s\n",
               ci->name, name);
  if ( name && !ci->name ) ci->name = name; 

  if ( gclass < min_gadget ) min_gadget = gclass;
  if ( gclass > max_gadget ) max_gadget = gclass;

  // Create a new Gadget structure on the list of gadgets.
  //
  Gadget* const gad = gadgets.pushi();

  gad->gclass = gclass;

  // Convert the gadget code from a ASCII hexadecimal string to
  // and array of bytes.
  //
  PStack<int> bytes;
  for ( const char *bp = bytes_text; *bp; )
    {
      const u_int8_t c = *bp;

      // Skip space.
      if ( c == ' ' ) { bp++; continue; }

      // Match any byte.
      if ( c == '*' )
        {
          bytes += GB_Wildcard;
          bp++;
          continue;
        }

      const int val = str_to_hex(bp);
      if ( val < 0 )
        {
          fprintf(stderr,"Problem in gadget at char %d in \"%s\"\n",
                  int(bp - bytes_text), bytes_text);
          gadgets.pop();
          return;
        }

      const bool range = bp[2] == '-';

      if ( range )
        {
          const int val2 = str_to_hex(bp+3);
          if ( val2 < 0 || val2 <= val )
            {
              fprintf(stderr,"Problem in gadget at char %d in \"%s\"\n",
                      int(bp + 3 - bytes_text), bytes_text);
              gadgets.pop();
              return;
            }

          bytes += GB_Range;
          bytes += val;
          bytes += val2;
          bp += 5;
        }
      else
        {
          // Insert the value in the array of bytes. ("+=" puts val on the
          // end of the list.)
          //
          bytes += val;

          // Advance the string pointer by two characters.
          bp += 2;
        }
    }

  gad->size = bytes.occ();
  gad->bytes = bytes.take_storage();
}


int
App::start(int argc, char **argv, char **envp)
{
  time_wall_start = time_wall_fp();
  page_size = getpagesize();
  page_mask = page_size - 1;

  // Default value.
  unlock_page_limit = 100;


  // setting MASK to 0111111111110000000 (need classes 2-12 to be Turing complete-avoiding XCHG classes)
  const u_int64_t mask_reg = 0x3ff80;

#define C(s) (u_int64_t(1)<<(s)) |
// Specify gadgets making up a Turing complete set. Must always end with 0.

  const u_int64_t mask_short = ( C(2) C(3) C(4) C(5) C(6) C(7) C(8) C(9) C(10) C(11) C(12) 0 );

#undef C

  gadget_avoid_mask = mask_short;

  //
  // Scan command-line arguments for options.
  //

  int argnum = 1;

  // Use the following options to get particular information when running our ropstop program.
  while ( argnum < argc )
    {
      const char* const arg = argv[argnum];
      if ( arg[0] != '-' ) break;
      argnum++;
      switch ( arg[1] ) {
      case 'l': // Limit on number of pages.
        unlock_page_limit = atoi(&arg[2]);
        break;
      case 'd': // Turn on debug info.
        debug = true;
        debug_msg_cnt = arg[2] ? atoi(&arg[2]) : 10;
        break;
      case 'r': // Show memory region information.
        show_regions = true;
        break;
      case 'g': // Show information on gadgets as they are found.
        gtune = true;
        break;

      default:
        pError_Msg("Usage: %s [-lPAGE_LIMIT] [-dMSG_LIMIT] [-r] [-g] prog args...\n",
                   argv[0]);
      }
    }

  never_lock = unlock_page_limit == -2;

  curr_regs_stale = true;

  gadgets_init();

  for ( int i=0; i<64; i++ )
    gadget_class_info[i].avoid_set = gadget_avoid_mask & u_int64_t(1) << i;

  // If no arguments provided, run a hello program, otherwise,
  // run the program specified on the command line.
  //
  const char *prog = argnum >= argc ? "./hello": argv[argnum];

  printf("ROP-Stop - Starting %s.\n",prog);


  //
  // Create child process, which will run prog.
  //

  pid = fork();  // Clone ourselves.

  if ( !pid )
    {
      // The copy executes here (pid == 0).
      PE( ptrace(PTRACE_TRACEME,0,NULL,NULL) );

      // Replace ourselves with the program provided on the command line.
      execve(prog, argv+argnum, envp);

      // If execution reaches here something went wrong.
      printf("Could not start program %s.\n",prog);
      exit(1);
    }


  //
  // Find and Lock Initial Set of Executable Regions in Child
  //

  // Run child until it returns the TRAP signal, used to return
  // control to us.
  while ( true )
    {
      int status;
      waitpid(pid,&status,0);

      if ( !WIFSTOPPED(status) ) { abort_tracing = true;  break; }

      const int sig = WSTOPSIG(status);
      if ( sig == SIGTRAP ) break;

      // Deliver signal to child.
      PE( ptrace(PTRACE_CONT,pid,NULL,sig) );
    }

  if ( abort_tracing ) return 1;

  // At this point program stopped by SIGTRAP, presumably inserted by ptrace.

  // Open file containing the executable image of child process.
  //
  pStringF proc_mem("/proc/%d/mem",pid);
  fd = open(proc_mem.s,O_RDONLY);
  if ( fd < 0 )
    pError_Msg("Could not open %s (%d)\n", proc_mem.s, errno);

  // Open file specifying where the executable regions of memory are
  // located.
  //
  pStringF proc_maps("/proc/%d/maps",pid);
  FILE* const maps = fopen(proc_maps,"r");
  if ( !maps )
    pError_Msg("Could not open memory map file %s: %s\n",
               proc_maps.s, strerror(errno));

  // Scan maps file for executable regions.
  //
  while ( !feof(maps) )
    {
      // Sample line in maps file:
      // 00400000-00401000 r-xp 00000000 fd:01 929724 /usr/bin/konsole
      pString str_low, str_high;
      while ( !feof(maps) )
        {
          const int c = fgetc(maps);
          if ( c == '-' ) break;
          str_low += c;
        }
      while ( !feof(maps) )
        {
          const int c = fgetc(maps);
          if ( c == ' ' ) break;
          str_high += c;
        }
      const int perm_r_c = fgetc(maps);
      const int perm_w_c = fgetc(maps);
      const int perm_x_c = fgetc(maps);
      if ( feof(maps) ) break;

      const size_t addr_low = strtol(str_low,NULL,16);
      const size_t addr_high = strtol(str_high,NULL,16);

      //  const bool perm_r = perm_r_c == 'r';
      //  const bool perm_w = perm_w_c == 'w';
      const bool perm_x = perm_x_c == 'x';

      if ( show_regions )
        printf(" 0x%lx - 0x%lx %c%c%c\n",
               addr_low, addr_high, perm_r_c, perm_w_c, perm_x_c);

      // This is an executable region of memory, remember it.
      //
      if ( perm_x && addr_high > addr_low ) region_new(addr_low,addr_high); 

      // Skip to the end of the line.
      while ( !feof(maps) ) if ( fgetc(maps) == '\n' ) break;
    }
  fclose(maps);

  {
    // Find a safe place to inject our code.
    //
    // THIS CODE DOES NOT REALLY FIND A SAFE PLACE.

    Region_Iter ri_last_ir = regions_locked.end();
    ri_last_ir--;
    Region_Info& ri_last = ri_last_ir->second;
    const int new_size = ri_last.size - page_size;
    ri_last.size = new_size;
    const size_t base_addr = ri_last.base_addr + new_size;
    Page_Info& pi = page_get(base_addr);
    pi.time_stamp = curr_time++;
    inj_addr_safe = base_addr; // Actually, we don't know.
    if ( new_size == 0 ) regions_locked.erase(ri_last_ir);
  }

  // Lock the executable regions.
  //
  for ( Region_Iter ir = regions_locked.begin();
        ir != regions_locked.end();  ir++ )
    {
      Region_Info& ri = ir->second;
      const int pages = ri.size / page_size;
      if ( show_regions )
        printf(" 0x%zx - 0x%x (%d pages)\n", ri.base_addr, ri.size, pages);
      if ( !never_lock )
        mprotect_child((void*)ri.base_addr, ri.size, PROT_READ);
    }

  if ( debug ) printf("Finished locking regions.\n");

  return main_loop();
}

int
App::main_loop()
{
  const bool single_step = false;

  void *sig_to_child = NULL;
  int debug_sig_cnt = 20;
  int curr_syscall = -1;  // When nonzero within a system call.
  user_regs_struct syscall_regs;
  PE( ptrace(PTRACE_GETREGS,pid,NULL,&syscall_regs) ); // Avoid warnings.

  iter_cnt = 0; // For debugging.

  printf("\nROP-Stop - Child Execution Beginning\n\n");

  PE( ptrace(PTRACE_SETOPTIONS, pid, NULL, PTRACE_O_TRACESYSGOOD) );


  //
  // Continue Running Child
  //

  while ( !abort_tracing )
    {
      iter_cnt++;
      curr_regs_stale = true;

      // If we modified child, set it back to its original state.
      //
      if ( child_modified )
        {
          child_modified = false;
          PE( ptrace(PTRACE_POKETEXT,pid,inj_addr_safe,child_save_text) );
          PE( ptrace(PTRACE_SETREGS,pid,NULL,&curr_regs) );
        }

      if ( debug && sig_to_child && debug_sig_cnt-- > 0 )
        {
          get_regs();
          printf("PC %#zx, delivering signal %zd to child.\n",
                 size_t(curr_regs.rip), size_t(sig_to_child));
        }

      // Resume Child Program
      //
      if ( single_step )
        { PE( ptrace(PTRACE_SINGLESTEP,pid,NULL,sig_to_child) ); }
      else
        { PE( ptrace(PTRACE_SYSCALL,pid,NULL,sig_to_child) ); }

      // Wait until child stops again.
      //
      int status;
      waitpid(pid,&status,0);

      // At this point child stopped again. Possible reasons:
      //
      //  Child encountered a page that we locked.
      //
      //  Child about to enter or exit a system call.
      //
      //  Child has normally exited.
      //
      //  Child has encountered some error.

      // Signal to send to child, initialize to no signal.
      sig_to_child = NULL;

      const ptrdiff_t sig_raw = WSTOPSIG(status);
      const ptrdiff_t sig = sig_raw & ~ size_t(0x80);

      // Program stopped.
      //
      if ( !WIFSTOPPED(status) ) { abort_tracing = true;  break; }

      switch ( sig ) {

      case SIGSEGV:
        // Segmentation violation (a.k.a., segfault, a memory issue) signal.
        //
        // The routine curr_page_process will check whether the segfault
        // was due to a page that we locked. If so, it will unlock
        // the page (possibly locking other pages), and return false.
        // If the segfault was due to some other cause, return true
        // and deliver the segfault signal to the child.
        //
        if ( curr_page_process() )
          sig_to_child = (void*) sig;  // Other segfault.
        break;

      case SIGTRAP:
        {
          sig_trap_cnt++;
          get_regs();
          const int64_t num = curr_regs.orig_rax;
          const bool syscall_entry = curr_syscall < 0;
          if ( syscall_entry )
            {
              syscall_regs = curr_regs;
              curr_syscall = num;
            }
          switch ( curr_syscall ){
          case SYS_open:
          case SYS_close:
          case SYS_read: case SYS_write:
          case 4: case 5: // newfstat
          case SYS_rt_sigaction:
          case SYS_ioctl:
            break;
          default:
            if ( false )
              printf("At PC %#zx syscall num %zd - %s\n",
                     size_t(curr_regs.rip), num, syscall_name[num]);
          }

          if ( !syscall_entry )
            {
              switch ( curr_syscall ) {
              case SYS_munmap:
              case SYS_mmap:
              case SYS_mprotect:
                if ( curr_syscall == SYS_munmap )
                  prot_count--; else prot_count++;
                if ( debug )
                printf("Syscall %3d %s %#zx - %#zx Mode %#zx\n",
                       curr_syscall, syscall_name[curr_syscall],
                       size_t(syscall_regs.rdi),
                       size_t(syscall_regs.rdi + syscall_regs.rsi),
                       size_t(syscall_regs.rdx));
                break;
              default:
                break;
              }

              if ( curr_syscall == SYS_mmap && !never_lock )
                handle_mmap
                  (syscall_regs.rdi, syscall_regs.rsi, syscall_regs.rdx);
              curr_syscall = -1;
            }
        }
        break;

      default:
        // Some other signal. Deliver it to the child.
        sig_to_child = (void*) sig;
        break;
      }
    }

  printf("\nROP-Stop - Finishing at %.6f s",
         time_wall_fp() - time_wall_start);
  if ( unlock_page_limit >= 0 )
    printf(", page limit %d.\n", unlock_page_limit);
  else if ( unlock_page_limit == -2 )
    printf(", pages never locked.\n");
  else
    printf(", avoid mask: %#zx\n", gadget_avoid_mask);
  printf(" Main Loop Iterations: %d  mprotect Calls %d  Trap Calls %d\n",
         iter_cnt, mprotect_child_count, sig_trap_cnt);
  printf(" Unlock, lock, scan count: %d,%d, %d.\n",
         unlock_count, lock_count, scan_count);

  printf(" %16s %16s %7s %8s\n",
         "Gadget", "Missing", "Static", "Unlocked");
  printf(" %16s %16s %7s %8s\n",
         "Vector", "Vector", "Pages", "Pages");

  PSList<Gadget_Stat*> unlock_sort;
  for ( Gadget_Stat_Iter gi = gadget_vec_stat.begin();
        gi != gadget_vec_stat.end(); gi++ )
    unlock_sort.insert(-gi->second.unlock_cnt,&gi->second);
  for ( Gadget_Stat *gi = NULL; 
        unlock_sort.iterate(gi) && unlock_sort.iterate_get_idx() < 10; )
    {
      const u_int64_t missing = 
        ( gi->vector & gadget_avoid_mask ^ gadget_avoid_mask ) 
        & gadget_avoid_mask;
      printf(" %#16zx %#16zx %7d %8d\n",
             gi->vector,
             missing,
             gi->page_cnt,
             gi->unlock_cnt);
    }

  printf("\nStatic Pages Containing Each Gadget Class\n\n");
  printf("%3s  %7s  %5s  %s\n", "Gad", "Static","Avoid","Description");
  printf("%3s  %7s\n", "Cls", "Pages");
  for ( int i = min_gadget; i <= max_gadget; i++ )
    {
      Gadget_Class_Info* const ci = &gadget_class_info[i];
      if ( ci->static_cnt )
        printf(" %2d  %7d  %5s  %s\n",
               i, ci->static_cnt, ci->avoid_set ? "Yes" : "   ", ci->name );
    }

  printf("The following gadgets were not found:\n");
  for ( int i = min_gadget; i <= max_gadget; i++ )
    {
      Gadget_Class_Info* const ci = &gadget_class_info[i];
      if ( ci->static_cnt ) continue;
      printf(" %2d  %7d  %5s  %s\n", 
             i, ci->static_cnt, ci->avoid_set ? "Yes" : "   ", ci->name );
    }

  printf("Average number unlocked pages:  ");
  if ( avg_cnt_samples == 0 )
    printf("Pages never-relocked.\n");
  else
    printf("%.1f.\n", double(avg_count)/avg_cnt_samples);

  return 0;
}

bool
App::curr_page_process()
{
  // First, assume that segfault due to execution in a locked page.

  get_regs();
  const size_t base_addr = page_round(curr_regs.rip);
  const size_t end_addr = curr_regs.rip + max_insn_size;
  const size_t end_addr_rp = page_round(end_addr);
  const int page_count = page_span(curr_regs.rip) ? 2 : 1;

  Region_Iter ir = regions_locked.upper_bound(curr_regs.rip);
  if ( ir == regions_locked.end() || !ir->second.in(end_addr) ) ir--;
  Region_Info& ri = ir->second;

  const size_t region_end = ri.base_addr + ri.size;

  const bool in_locked_region = ri.in( curr_regs.rip ) || ri.in( end_addr );

  Page_Info* const pi = &page_get(base_addr);
  Page_Info* const pi2 = page_count > 1 ? &page_get(end_addr_rp) : NULL;
  const bool pi_in_region = ri.in(pi->base_addr);
  const bool pi2_in_region = pi2 && ri.in(pi2->base_addr);
  assert( in_locked_region || !pi_in_region && !pi2_in_region );

  if ( in_locked_region )
    {
      if ( debug && ( page_count > 1 || debug_msg_cnt-- > 0 ) )
        printf("PC %#zx in locked region %#zx - %#zx\n",
               size_t(curr_regs.rip), ri.base_addr, region_end);
      assert( pi_in_region || pi2_in_region );
      const int page_offset =
        curr_regs.rip < ri.base_addr + page_size
                        ? 0 : ( base_addr - ri.base_addr ) / page_size;
      const size_t region_b_base = end_addr_rp + page_size;
      const int region_b_size = region_end - region_b_base;

      // Unlock entire region.
      mprotect_child((void*)ri.base_addr, ri.size, PROT_READ | PROT_EXEC );

      if ( page_offset == 0 ) regions_locked.erase(ir);
      else
        {
          // Make region smaller, then lock it.
          ri.size = base_addr - ri.base_addr;
          mprotect_child((void*)ri.base_addr, ri.size, PROT_READ );
        }
      if ( region_b_size > 0 )
        {
          Region_Info& ri_new = regions_locked[region_b_base];
          ri_new.base_addr = region_b_base;
          ri_new.size = region_b_size;
          assert( page_offset == 0
                  || ri.base_addr + ri.size + page_size * page_count
                  == ri_new.base_addr );
          mprotect_child((void*)ri_new.base_addr, ri_new.size, PROT_READ );
        }

      if ( pi_in_region ) page_unlock(pi);
      if ( pi2_in_region ) page_unlock(pi2);
    }

  if ( !in_locked_region && ( !pi->is_locked && ( !pi2 || !pi2->is_locked ) ) )
    {
      // The page containing the instruction was not locked by us.
      // Check the address of the segfault (which might not be
      // the PC), and maybe use it in an error message.

      siginfo_t sig_info;
      PE( ptrace(PTRACE_GETSIGINFO,pid,NULL,&sig_info) );

      Region_Info* const ri_data = region_lookup((size_t)sig_info.si_addr);

      printf
        ("PC %#zx segfault at %p (sig %d) not due to locked page?\n",
         curr_regs.rip, sig_info.si_addr, sig_info.si_signo);

      if ( ri_data )
        printf("   In Region %#zx - %#zx\n",
               ri_data->base_addr, ri_data->end_addr());
      else
        printf("   Not in a locked region.\n");
      return true;
    }

  if ( pi->is_locked ) page_unlock(pi);
  if ( pi2 && pi2->is_locked ) page_unlock(pi2);
 
  // If necessary, lock pages.
  //
  const int lock_count_before = lock_count;

  if ( unlock_page_limit < 0 )
    {
      // Lock pages to avoid a Turing complete set.

      u_int64_t unlocked_vector = 0;

      unlocked_vector |= pi->gadget_vector;
      if ( pi2 ) unlocked_vector |= pi2->gadget_vector;

      PSList<Page_Info*> age_sort;
      for ( Page_Iter i = pages_unlocked.begin();
            i != pages_unlocked.end(); i++ )
        {
          const size_t p_addr = *i;
          Page_Info* const pi = &page_get(p_addr);
          age_sort.insert(pi->unlock_cnt,pi);
        }

      for ( Page_Info *pi = NULL; age_sort.iterate(pi); )
        {
          const u_int64_t next_vector = unlocked_vector | pi->gadget_vector;

          if ( ( next_vector & gadget_avoid_mask ) == gadget_avoid_mask )
            {
              // Unlocked set is Turing complete with pi, so lock it.
              page_mp_lock(pi);
            }
          else
            {
              unlocked_vector = next_vector;
            }
        }

#if 0              
          //////////////////////////////////////////
          bool byteCompare[19];  // Array to hold result of masking.
          int mask[19] = {0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0};  // Turing complete if gadgets in classes 2-17
          for (int index = 0; index < 19; index++)  {  
            byteCompare[index] = (i || mask[index]);  // Apply current bit of page (at i) to current bit of mask and store result. 
          }
          bool eval = true;  // Used to see if mask and result are equivalent.
          for (int index = 0; index < 19, eval != false; index++)  {
            if(byteCompare[index] == mask[index])  {  // See if mask and result of OR operation are equivalent.
              continue;
            }
            else  {
              eval = false;  // mask and result not Turing complete; no need to lock anything
            }
          }

          if (eval == true)  {  // if mask and result equivalent
            // Need to lock recently occurring page->the page we just unlocked prior to the current page (on premise that if it just occurred, it might mean this page            //  is frequently occurring.) 
          }
          /////////////////////////////////////////
#endif

    }
  else
    {
      // Lock pages only to satisfy unlock_page_limit, we are ignoring
      // Turing completeness. Note: unlock_page_limit is intended for
      // testing performance.
      //


      while ( int(pages_unlocked.size()) >= unlock_page_limit )
        {
          int time_oldest = curr_time;
          size_t addr_oldest = 0;

          for ( Page_Iter i = pages_unlocked.begin();
                i != pages_unlocked.end(); i++ )
            {
              const size_t v_addr = *i;
              Page_Info& vi = page_get(v_addr);
              if ( v_addr == pi->base_addr ) continue;
              if ( pi2 && v_addr == pi2->base_addr ) continue;
              if ( vi.time_stamp < time_oldest )
                {
                  time_oldest = vi.time_stamp;
                  addr_oldest = v_addr;
                }
            }

          Page_Info& oi = page_get(addr_oldest);
          page_mp_lock(&oi);
        }
    }

  if ( lock_count_before != lock_count )
  {
    avg_cnt_samples++;
    avg_count += int(pages_unlocked.size());
  }

  return false;
}

void
App::page_mp_lock(Page_Info *pi)
{


  if ( debug && debug_msg_cnt-- > 0 )  {
    printf("PC %#zx, locking %#zx 1 pages.\n",
           curr_regs.rip, pi->base_addr);
  }
  
  mprotect_child((void*)pi->base_addr, page_size, PROT_READ );
  pi->is_locked = true;
  pages_unlocked.erase(pi->base_addr);
  lock_count++;
}

void
App::page_unlock(Page_Info *pi)
{
  unlock_count++;
  if ( debug && debug_msg_cnt-- > 0 )
    printf("PC %#zx, unlocking %#zx.\n",
           curr_regs.rip, pi->base_addr);

  if ( !pi->page_scanned ) page_scan(pi);

  if ( pi->is_locked )
    mprotect_child((void*)pi->base_addr, page_size, PROT_READ | PROT_EXEC );
  else
    { assert( pi->time_stamp == 0 ); }

  pi->time_stamp = curr_time++;
  pi->is_locked = false;
  pi->unlock_cnt++;
  pi->gadget_stat->unlock_cnt++;
  pages_unlocked.insert(pi->base_addr);
}

// Scan page for gadgets.
//
void
App::page_scan(Page_Info *pi)
{
  scan_count++;

  // Size of page plus an  extra amount for gadgets that cross page boundaries.
  const int page_size_plus = page_size + max_insn_size;
  u_int8_t page[page_size_plus];

  // Load the page.
  lseek(fd, off_t(pi->base_addr), SEEK_SET);
  read(fd, page, page_size_plus);

  u_int64_t vector = 0;
  const int num_gadgets = gadgets.occ();

  // Scan the page byte-by-byte.  This can be optimized.
  //
  for ( int i=0; i<page_size; i++ )
    {
      for ( int g=0; g<num_gadgets; g++ )
        {
          Gadget* const gad = &gadgets[g];
          bool match_so_far = true;

          u_int8_t  *pptr = &page[i];

          for ( int p=0; p<gad->size && match_so_far; p++ )
            {
              const int byte = *pptr++;
              if ( gad->bytes[p] == GB_Range )
                {
                  const int low = gad->bytes[++p];
                  const int high = gad->bytes[++p];
                  if ( byte < low || byte > high ) match_so_far = false;
                }
              else if ( gad->bytes[p] == GB_Wildcard )
                {
                  // Do nothing.
                }
              else if ( gad->bytes[p] != byte )
                {
                  match_so_far = false;
                }
            }

          if ( !match_so_far ) continue;

          const u_int64_t bit = u_int64_t(1) << gad->gclass;

          // If we already found this class of gadget on this page don't
          // update the vector or statistics.
          if ( vector & bit ) continue;

          // Matching gadget.
          vector |= u_int64_t(1) << gad->gclass;
          gadget_class_info[gad->gclass].static_cnt++;
        }
    }

  Gadget_Stat* const gs = &gadget_vec_stat[vector];
  gs->vector = vector;
  gs->page_cnt++;
  pi->gadget_stat = gs;

  if ( gtune )
    printf("For Page %#zx, Gadget Vector %016zx (iter %d) Pages %d\n",
           pi->base_addr, vector, iter_cnt, gs->page_cnt);

  pi->gadget_vector = vector;
  pi->page_scanned = true;
}


int
main(int argc, char **argv, char **envp)
{
  App app;
  return app.start(argc,argv,envp);
}
