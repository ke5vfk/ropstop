void
App::gadgets_init()
{
  // Specify gadgets to scan for.

  // The first argument is gadget class.
  // Two gadgets of the same class are considered interchangeable.
  //
  // Second argument is the gadget code, specified byte-by-byte
  // a string of space-separated hexadecimal numbers (without the 0x).
  //
  // Third argument is the name for the instruction used within
  // the particular class.

  /* Gadget classes 2-18 based on the gadgets described by Homescu et al,
     "Microgadgets: Size Does Matter in Turing-complete Return-oriented
     Programming," USENIX Workshop on Offensive Technologies, 2012.

     Gadget 19 added by Morgan Burcham. */

  // Gadget Sets:

  //  Straight-Line Code: 2,3,4,7,8

  //  Turing Complete 2-18

  //  Turing Complete, conservative: 2-9, 11, 13-18

  gadget_insert(1, "c3", "Return");

  gadget_body_insert(2, "ff c0-c7", "Increment/Decrement");
  gadget_body_insert(2, "ff c8-cf");
  gadget_body_insert(2, "04 *"); // al += imm8
  gadget_body_insert(2, "80 c0-c7 *"); // r/m8 += imm8
  gadget_body_insert(2, "83 c0-c7 *"); // r/m32 += imm8

  gadget_body_insert(3, "58-5f", "POP");

  gadget_body_insert(4, "01 c1-fe", "ADD/ADC/SUB/SBB");
  gadget_body_insert(4, "48 01 c1-fe");
  gadget_body_insert(4, "48 11 c1-fe");
  gadget_body_insert(4, "48 19 c1-fe");
  gadget_body_insert(4, "48 29 c1-fe");

  gadget_body_insert(5, "31 c1-fe", "XOR/NOT/NEG");
  gadget_body_insert(5, "f7 d0-d7");
  gadget_body_insert(5, "f7 d8-df");

  gadget_body_insert(6, "09 c1-fe", "OR/AND");
  gadget_body_insert(6, "21 c1-fe");

  // Load from memory.
  gadget_body_insert(7, "ad", "Load from Memory"); // LODSD
  gadget_body_insert(7, "8b 00-3f");    // Load. (Later take out sp cases.)
  gadget_body_insert(7, "8b 40-7f *");  // Load 1-byte displ.
  gadget_body_insert(7, "8b 80-bf * *");  // Load 2-byte displ.

  // Store to memory.
  gadget_body_insert(8, "ab", "Store to Memory"); // STOSD
  gadget_body_insert(8, "89 00-3f");  // Store.  (Later take out sp cases.)
  gadget_body_insert(8, "89 40-7f *");  // Store, 1-byte displ
  gadget_body_insert(8, "89 80-bf * *");  // Store, 2-byte displ

  // Clear carry flag.
  gadget_body_insert(9, "9e", "Clear Flag"); // SAHF
  gadget_body_insert(9, "f8"); // CLC

  // Load flags.
  // LAHF  (Not valid in 64-b mode) */
  //  gadget_body_insert(10, "9f", "Get Flags"); LAHF

  gadget_body_insert(10, "9c 50-57", "Get Flags"); // PUSHF; PUSH reg.

  // Write RSP.
  //
  gadget_body_insert(11, "48 94", "Write RSP"); // XCHG RAX,RSP
  gadget_body_insert(11, "c9"); // LEAVE: rsp <- rbp; pop rbp
  gadget_body_insert(11, "5c"); // POP RSP.
  gadget_body_insert(11, "48 89 c4"); // MOV RSP = reg
  gadget_body_insert(11, "48 89 dc"); // MOV r/m = reg;
  gadget_body_insert(11, "48 89 cc");
  gadget_body_insert(11, "48 89 d4");
  gadget_body_insert(11, "48 89 ec");
  gadget_body_insert(11, "48 89 f4");
  gadget_body_insert(11, "48 89 fc");
  gadget_body_insert(11, "48 8b e0-e3"); // MOV reg = r/m
  gadget_body_insert(11, "48 8b e5-e7");

  // PUSHA: Not valid in 64-bit mode.


  // Read RSP.
  //
  gadget_body_insert(12, "48 89 e0-e3", "Read RSP"); // MOV reg,RSP
  gadget_body_insert(12, "48 89 e5-e7");

  gadget_body_insert(12, "48 8b c4"); // MOV reg = RSP
  gadget_body_insert(12, "48 8b dc"); // MOV reg = r/m
  gadget_body_insert(12, "48 8b cc");
  gadget_body_insert(12, "48 8b d4");
  gadget_body_insert(12, "48 8b ec");
  gadget_body_insert(12, "48 8b f4");
  gadget_body_insert(12, "48 8b fc");

  gadget_body_insert(12, "48 94");  // XCHG RAX,RSP

  gadget_body_insert(12, "48 03 c4"); // ADD r64 += r/m64 : r/m64->RSP
  gadget_body_insert(12, "48 03 dc");
  gadget_body_insert(12, "48 03 cc");
  gadget_body_insert(12, "48 03 d4");
  gadget_body_insert(12, "48 03 ec");
  gadget_body_insert(12, "48 03 f4");
  gadget_body_insert(12, "48 03 fc");

  gadget_body_insert(12, "48 03 e0-e3"); // ADD r/m += reg : reg -> RSP
  gadget_body_insert(12, "48 03 e5-e7");


  gadget_body_insert(13, "48 93", "XCHG RAX, RBX");
  gadget_body_insert(14, "48 91", "XCHG RAX, RCX");
  gadget_body_insert(15, "48 92", "XCHG RAX, RDX");
  gadget_body_insert(16, "48 95", "XCHG RAX, RBP");
  gadget_body_insert(17, "48 96", "XCHG RAX, RSI");
  gadget_body_insert(18, "48 97", "XCHG RAX, RDI");

  // Added by Morgan Burcham.
  gadget_body_insert(19, "50-57", "PUSH");

  // Added by Dr. David Koppelman.
  gadget_insert(24, "ff d0-d7", "CALLQ *reg");

}
