
default: all

all: rop-stop hello

CXX = g++
CXXFLAGS = -Wall  -g  -Wno-parentheses -O3
LINKFLAGS = -lrt

SRCS = rop-stop.cc

OBJ_FILES = $(SRCS:.cc=.o) rop-stop-hand.o

hello: h.cc Makefile
	$(CXX) -Wall -O3 -g $< -o $@
	objdump -d $@ > $@-dis.s

rop-stop.o: gadgets.h

%.o: %.S Makefile
	as $< -o $@

%.o: %.cc Makefile
	$(CXX) $(CXXFLAGS) -c $<

rop-stop: $(OBJ_FILES)
	$(CXX) $(LINKFLAGS) -o $@ $^
	objdump -d $@ > $@-dis.s

clean:
	rm -f *.s *-dis.s *.o hello rop-stop *~ tex/*.dvi tex/*.log bm/166.s \
	rop-stop.o.bz2
